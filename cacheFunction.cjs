function cacheFunction(cb) {
    if (typeof cb !== 'function') {
        throw new Error("wrong input");
    }

    const cache = {};
    function invoke(...args) {

        const numOfArg = JSON.stringify(args);

        if (numOfArg in cache) {
            return cache[numOfArg];
        }
        else {
            const ans = cb(...args);
            cache[numOfArg] = ans;
            return ans;
        }

    }
    return invoke;

}

module.exports = cacheFunction;