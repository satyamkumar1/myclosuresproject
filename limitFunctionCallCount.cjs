function limitFunctionCallCount(cb, numInput) {

    if(numInput<0 || typeof cb!=='function' || typeof numInput !=='number'){
        throw new Error("wrong input");
    }

    let countnum=0;
    function invoke(...args) {

       
            if (numInput<=countnum) {
                
                return null;
            }
            else {
                countnum++;

                return cb(...args);
            } 



    }
    return invoke;


}
module.exports = limitFunctionCallCount;