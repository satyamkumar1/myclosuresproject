const getData = require('../limitFunctionCallCount.cjs');

function callBack(...args) {
    console.log(args);
}
try {
    const getCallbackData = getData(callBack, 2);

    getCallbackData();
    getCallbackData();
    getCallbackData();
    getCallbackData();
}
catch (error) {
    console.log(error);
}


