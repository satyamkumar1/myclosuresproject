function counterFactory() {
    let value = 0;

    function increment() {
        return ++value;
    }
    function decrement() {
        return --value;
    }
    // increment();
    // decrement();
    return { increment, decrement };

}

module.exports = counterFactory;